import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BarraLateralComponent } from './barra-lateral/barra-lateral.component';
import { LayoutModule } from '@angular/cdk/layout';

import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { AlertsComponent } from './alerts/alerts.component';
import { StateComponent } from './state/state.component';
import { HystoryComponent } from './hystory/hystory.component';
import { ApiComunicationComponent } from './api-comunication/api-comunication.component';
import { InfoComponent } from './info/info.component';
import { MatSelectModule } from '@angular/material/select';
import {FormsModule} from '@angular/forms';
import { LineletterPipe } from './lineletter.pipe';
import { DateFormaterPipe } from './date-formater.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BarraLateralComponent,
    AlertsComponent,
    StateComponent,
    HystoryComponent,
    ApiComunicationComponent,
    InfoComponent,
    LineletterPipe,
    DateFormaterPipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatSelectModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
