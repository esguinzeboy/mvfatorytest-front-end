import { Component, OnInit } from '@angular/core';
import { GetfromapiService } from '../getfromapi.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {

  private states = null;
  lines = null;
 
  showLines: string[] = [];
  showStation: string[] = [];
  showDirections: string[] = [];
 
  selectedLine: string = "";
  selectedStation: string = "";
  selectedDirection: string = "";

  showTime:string="";
  

  constructor(public getfromapiService: GetfromapiService, public http: HttpClient) { }

  ngOnInit() {

    this.getTrips()
    this.getLines();
  }

  getTrips() {
    this.getfromapiService.getTrips()
      .subscribe(result => this.states = result)
      }

  getLines() {
    this.getfromapiService.getLines()
      .subscribe(result => {
      this.lines = result;
        this.linesToShow();
      });
  }

  upload() {
    this.getTrips();
    this.linesToShow();
  }

  linesToShow() {

    var findOption = false;
    var countLines = this.lines.length;
    var showLineshere = this.showLines;
    for (let index = 0; index < countLines; index++) {
      findOption = showLineshere.some(e => e === this.lines[index].Route_Id);

      if (!findOption) {
        this.showLines.push(this.lines[index].Route_Id)
      }

    }

  }

  stationToshow() {
    
    if (this.selectedLine !== "") {
      this.showStation=[];
      var lineToUse =this.states.find(e=> e.Route_Id===this.selectedLine)
     
      for (let index = 0; index < lineToUse.Estaciones.length; index++) {
        
          this.showStation.push(lineToUse.Estaciones[index].stop_name);
     }
    }
  }

  directionToShow() {
    if (this.selectedLine !== "") {
    this.showDirections=[];
    this.showDirections=this.lines.filter(x=> x.Route_Id==this.selectedLine);
    }
  }

  selecteLine() {
    
    this.selectedStation  = "";
    this.selectedDirection="";
    this.stationToshow();
    this.directionToShow();
  }

  find()
  {
   
if(this.selectedLine==="" || this.selectedDirection===""||this.selectedStation==="")
{
   alert("Por Favor selecione los tres campos");
}
else{

   var stationSelected;
   var lineFilter=this.states.filter(x=>x.Route_Id===this.selectedLine && x.Direction_ID==this.selectedDirection.toString());
  // var stationFilter= lineFilter.Estaciones.find(x=>x.Estaciones.stop_name.toString()===this.selectedStation);
   var stations= lineFilter[0].Estaciones;
   for (let index = 0; index < stations.length; index++) {
    if(stations[index].stop_name.toString() === this.selectedStation)
    {
      stationSelected=stations[index];
    } 
   }
   this.showTime=stationSelected.arrival.time;

  }
}
}
