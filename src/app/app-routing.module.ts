import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertsComponent } from './alerts/alerts.component';
import { StateComponent } from './state/state.component';
import { HystoryComponent } from './hystory/hystory.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  {
    path:'Alerts',
    component:AlertsComponent
  },
  {
    path:'State',
    component:StateComponent
  },
  {
    path:'History',
    component:HystoryComponent
  } ,
  {
    path:'Info',
    component:InfoComponent
  } 
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
