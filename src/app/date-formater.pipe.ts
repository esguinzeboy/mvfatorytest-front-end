import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormater'
})
export class DateFormaterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (typeof value !== "undefined"){
  
    var hours= value.toString().substr(8, 2);
    var minutes= value.toString().substr(10, 2);;
    return hours+":"+minutes;
  }
   
  }

}
