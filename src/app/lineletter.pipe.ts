import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lineletter'
})
export class LineletterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
     
    return value.substr(value.length - 1);

  }

}
