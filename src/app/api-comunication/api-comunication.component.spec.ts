import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiComunicationComponent } from './api-comunication.component';

describe('ApiComunicationComponent', () => {
  let component: ApiComunicationComponent;
  let fixture: ComponentFixture<ApiComunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiComunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiComunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
