import { Component, OnInit } from '@angular/core';

import { GetfromapiService } from '../getfromapi.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  private alerts = null;
  lines = null;
  showLines: string[] = [];
  showAlerts = [];

  constructor(public getfromapiService: GetfromapiService, public http: HttpClient) 
  { 

    
  }

  ngOnInit() {

    this.getAlerts()
    this.getLines();
    //this.alertToShow();
    
  }

  getAlerts() {
    this.getfromapiService.getAlerts()
      .subscribe(
        data => { this.alerts = data },
        err => console.log(err),
        () => console.log('done alerts'));

  }

  getLines() {
    this.getfromapiService.getLines()
      .subscribe(result => {
        this.lines = result;
        this.linesToShow()
      });
  }

  linesToShow() {

    var findOption = false;
    var countLines = this.lines.length;
    var showLineshere = this.showLines;
    for (let index = 0; index < countLines; index++) {
      findOption = showLineshere.some(e => e === this.lines[index].Route_Id);

      if (!findOption) {
        this.showLines.push(this.lines[index].Route_Id)
      }

    }

    this.alertToShow()
  }

  alertToShow() {
    
    if (this.alerts !== null) {
      var getBadAlerts = [{ lineName: '', description: '' }];
      getBadAlerts = [];
      this.showAlerts = [];
    for (let index = 0; index < this.showLines.length; index++) {
      this.showAlerts.push({ lineName: this.showLines[index], description: "Funciona con normalidad." });
    }

      for (let index = 0; index < this.alerts.length; index++) {

        var Linea = this.alerts[index].informed_entity[0].route_id;
        var mensaje = this.alerts[index].description_text[0].text;
        getBadAlerts.push({ lineName: Linea, description: mensaje })
      }



      for (let index = 0; index < getBadAlerts.length; index++) 
      {
        for (let index2 = 0; index2 < this.showLines.length; index2++) 
        {
          if (getBadAlerts[index].lineName === this.showLines[index2]) 
          {
            this.showAlerts[index2]=getBadAlerts[index];
          }

        }
      }

    }

  }

}
