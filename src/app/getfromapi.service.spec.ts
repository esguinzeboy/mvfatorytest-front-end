import { TestBed } from '@angular/core/testing';

import { GetfromapiService } from './getfromapi.service';

describe('GetfromapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetfromapiService = TestBed.get(GetfromapiService);
    expect(service).toBeTruthy();
  });
});
