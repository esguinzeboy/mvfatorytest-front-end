import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
@Injectable({
  providedIn: 'root'
})
export class GetfromapiService {

  constructor(private http: HttpClient) { }

     getAlerts() {
    return  this.http.get("https://localhost:44327/api/SubteExt/alerts");
  } 
  getTrips() {
    return this.http.get("https://localhost:44327/api/SubteExt/trips");
  }
  getHistory() {
    return this.http.get("https://localhost:44327/api/Alert");
  }
  getEffect() {
    return this.http.get("https://localhost:44327/api/Effect");
  }
  getCause() {
    return this.http.get("https://localhost:44327/api/Cause");
  }
  
  getLines() {
    return this.http.get("https://localhost:44327/api/Line/Full");
  }
  //getLineFull():Observable<string>{ return this.http.get<string>("https://localhost:44327/api/Line/Full")}
}
